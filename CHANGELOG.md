# Changelog

All notable changes to `laravel-visits` will be documented in this file.

## 1.0.0 - 2022-04-12

- Initial release
