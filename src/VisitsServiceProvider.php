<?php

namespace Quantumh\Visits;

use Carbon\Carbon;
use Config;
use Illuminate\Contracts\Http\Kernel as HttpKernel;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Quantumh\Visits\Http\Middleware\SetVisitorUuidCookie;
use Quantumh\Visits\Http\Middleware\TrackVisit;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;


class VisitsServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     */
    public function boot(): void
    {
        $this->publishes([
            __DIR__ . '/../config/visits.php' => config_path('visits.php'),
        ],'quantum-visits-config');

        // Publish Spatie Permissions migrations
        $this->publishes([
            __DIR__ . '/../database/migrations/create_tracker_visits_table.php.stub' => $this->getMigrationFileName("create_tracker_visits_table.php"),
            __DIR__ . '/../database/migrations/alter_table_tracker_visits_add_region.php.stub' => $this->getMigrationFileName("alter_table_tracker_visits_add_region.php"),
        ], 'quantum-visits-migrations');

        if (Config::get('visits.enabled')) {
            $this->registerMiddleware();
        }
    }

    /**
     * Register the Vapor HTTP middleware.
     *
     * @return void
     */
    protected function registerMiddleware()
    {
        $middlewareTrackVisit = config('visits.middleware.track_visit', TrackVisit::class);
        $middlewareSetVisitorUuidCookie = config('visits.middleware.set_visitor_uuid_cookie', SetVisitorUuidCookie::class);

        $this->app[HttpKernel::class]->appendMiddlewareToGroup('web', $middlewareSetVisitorUuidCookie);
        $this->app[HttpKernel::class]->appendMiddlewareToGroup('web', $middlewareTrackVisit );
    }

    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @return string
     */
    protected function getMigrationFileName($migrationFileName): string
    {
        $timestamp = date('Y_m_d_His');

        $filesystem = $this->app->make(Filesystem::class);

        return Collection::make($this->app->databasePath().DIRECTORY_SEPARATOR.'migrations'.DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem, $migrationFileName) {
                return $filesystem->glob($path.'*_'.$migrationFileName);
            })
            ->push($this->app->databasePath()."/migrations/{$timestamp}_{$migrationFileName}")
            ->first();
    }

}
