<?php

namespace Quantumh\Visits\Http\Middleware;

use Closure;
use Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Quantumh\Visits\MobileDetect;
use Quantumh\Visits\Models\TrackerAgent;
use Quantumh\Visits\Models\TrackerDevice;
use Quantumh\Visits\Models\TrackerGeoIp;
use Quantumh\Visits\Models\TrackerLanguage;
use Quantumh\Visits\Models\TrackerReferer;
use Quantumh\Visits\Models\TrackerVisit;
use Stevebauman\Location\Facades\Location;
use UAParser\Parser;
use Illuminate\Support\Facades\DB;

class TrackVisit
{
    protected $except = [];

    public function handle($request, Closure $next)
    {
        $this->except = [
            'livewire/*',
        ];

        $cookieName = Config::get('visits.tracker_cookie_name');
        $uuid = $request->cookie($cookieName);

        if ($this->shouldPassThrough($request)) {
            return $next($request);
        }

        $mDetector = new MobileDetect();
        $visitor = $request->visitor();
        $userAgent = $this->setUserAgentData($visitor);
        $device = $this->setDeviceData($visitor, $mDetector);
        $referer = $this->setRefererData($visitor);
        $language = $this->setLanguageData($visitor);
        $geoIp = $this->setGeoIpData($visitor);

        $user = Auth::user()?->{Config::get('visits.auth_table_primary_key', 'id')};
        $regionalColumnName = Config::get('visits.auth_table_regionable_column', 'region_id');
        $userTable = Auth::user() ? Auth::user()->getTable() : null;
        $destination = $request->fullUrl();
        try {
            TrackerVisit::create([
                'uuid' => $uuid,
                'destination' => $destination,
                'agent_id' => $userAgent?->id,
                'device_id' => $device?->id,
                'referer_id' => $referer?->id,
                'language_id' => $language?->id,
                'geoip_id' => $geoIp?->id,
                'is_robot' => $mDetector->isRobot(),
                'client_ip' => $visitor->ip(),
                'user_id' => $user,
                'user_table' => $userTable,
                $regionalColumnName => session()->get($regionalColumnName),
            ]);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            if (app()->bound('sentry')) {
                app('sentry')->captureException($ex);
            }
        }

        return $next($request);
    }

    public function shouldPassThrough($request)
    {
        foreach ($this->except as $except) {
            if ($request->is($except)) {
                return true;
            }
        }
        return false;
    }

    public function setUserAgentData($visitor)
    {
        $parser = Parser::create()->parse($visitor->userAgent());
        $userAgent = $parser->ua;
        $originalUserAgent = $parser->originalUserAgent;
        $userAgentVersion = $userAgent->major.
            ($userAgent->minor !== null ? '.'.$userAgent->minor : '').
            ($userAgent->patch !== null ? '.'.$userAgent->patch : '');

        $nameHash = hash('sha256', $originalUserAgent);

        try {
            return TrackerAgent::create([
                'name' => $originalUserAgent,
                'browser' => $userAgent->family,
                'browser_version' => $userAgentVersion,
                'name_hash' => $nameHash,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->errorInfo[1] == 1062) { // Check for duplicate entry error code
                // Retrieve the existing record after catching the exception
                return TrackerAgent::where('name_hash', $nameHash)->firstOrFail();
            }
            Log::error("Error inserting into TrackerAgent: ".$e->getMessage());
        }
    }

    public function setRefererData($visitor)
    {
        $url = parse_url($visitor->referer());
        if (!isset($url['host'])) {
            return;
        }

        $parts = explode('.', $url['host']);
        $domain = array_pop($parts);
        if (count($parts) > 0) {
            $domain = array_pop($parts).'.'.$domain;
        }

        $parser = new \Snowplow\RefererParser\Parser();
        $referer = $parser->parse(
            $visitor->referer(),
            $url['host']
        );

        $medium = $source = $searchTerm = null;
        if ($referer->isKnown()) {
            $medium = $referer->getMedium(); // "Search"
            $source = $referer->getSource(); // "Google"
            $searchTerm = $referer->getSearchTerm();   // "gateway oracle cards denise linn"
        }

        return TrackerReferer::firstOrCreate([
            'url' => \Illuminate\Support\Str::substr($visitor->referer(), 0, 254),
            'host' => $url['host'],
            'domain' => $domain,
            'medium' => $medium,
            'source' => $source,
            'search_terms_hash' => sha1($searchTerm)
        ]);
    }

    public function setDeviceData($visitor, $mDetector)
    {
        $deviceDetails = $mDetector->detectDevice();

        $parser = Parser::create()->parse($visitor->userAgent());

        $operatingSystem = $parser->os;
        $operatingSystemVersion = $operatingSystem->major.
            ($operatingSystem->minor !== null ? '.'.$operatingSystem->minor : '').
            ($operatingSystem->patch !== null ? '.'.$operatingSystem->patch : '');

        $device = !is_null($parser->device->family) ? $parser->device->family : 'Unknown';
        $deviceBrand = !is_null($parser->device->brand) ? $parser->device->brand : 'Unknown';
        $deviceModel = !is_null($parser->device->model) ? $parser->device->model : 'Unknown';

        // Use query builder to handle potential race conditions
        return DB::transaction(function () use (
            $deviceDetails,
            $deviceModel,
            $deviceBrand,
            $operatingSystem,
            $operatingSystemVersion
        ) {
            // Try to find an existing record first
            $existingDevice = TrackerDevice::where([
                'kind' => $deviceDetails['kind'],
                'model' => $deviceModel,
                'brand' => $deviceBrand,
                'platform' => $operatingSystem->family,
                'platform_version' => $operatingSystemVersion,
            ])->first();

            // If device exists, return it
            if ($existingDevice) {
                return $existingDevice;
            }

            // Create new device record with lock to prevent race conditions
            return TrackerDevice::create([
                'kind' => $deviceDetails['kind'],
                'model' => $deviceModel,
                'brand' => $deviceBrand,
                'platform' => $operatingSystem->family,
                'platform_version' => $operatingSystemVersion,
                'is_mobile' => $deviceDetails['is_mobile'],
            ]);
        });
    }

    public function setLanguageData($visitor)
    {
        $languages = $visitor->languages();
        $preference = count($languages) ? $languages[0] : 'en';
        $range = implode(',', $visitor->languages());

        return TrackerLanguage::firstOrCreate([
            'preference' => $preference,
            'language_range' => $range,
        ]);
    }

    public function setGeoIpData($visitor)
    {
        $ip = $visitor->ip();
        $location = Location::get($ip);

        return TrackerGeoIp::firstOrCreate([
            'country_code' => $location ? $location->countryCode : null,
            'country_name' => $location ? $location->countryName : null,
            'region_code' => $location ? $location->regionCode : null,
            'region_name' => $location ? $location->regionName : null,
            'city_name' => $location ? $location->cityName : null,
            'zip_code' => $location ? $location->zipCode : null,
            'iso_code' => $location ? $location->isoCode : null,
            'postal_code' => $location ? $location->postalCode : null,
            'latitude' => $location ? $location->latitude : null,
            'longitude' => $location ? $location->longitude : null,
            'metro_code' => $location ? $location->metroCode : null,
            'area_code' => $location ? $location->areaCode : null,
            'timezone' => $location ? $location->timezone : null,
        ]);
    }

}