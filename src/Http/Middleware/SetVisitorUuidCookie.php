<?php

namespace Quantumh\Visits\Http\Middleware;

use Closure;
use Illuminate\Support\Str;
use Config;

class SetVisitorUuidCookie
{

    public function handle($request, Closure $next)
    {
        $cookieName = Config::get('visits.tracker_cookie_name');
        if (!$request->hasCookie($cookieName) && $request->method() == 'GET') {
            $uuid = Str::uuid()->toString();
            $response = $next($request);
            return $response->withCookie(cookie()->forever($cookieName, $uuid));
        }
        return $next($request);
    }
}
