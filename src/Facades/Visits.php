<?php

namespace Quantumh\Visits\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Quantumh\Visits
 */
class Visits extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'visits';
    }
}
