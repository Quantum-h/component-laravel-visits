<?php

namespace Quantumh\Visits\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class TrackerVisit extends Model
{
    protected $table = 'tracker_visits';

    protected $guarded = [];

    public function geoIp()
    {
        return $this->belongsTo(TrackerGeoIp::class, 'geoip_id', 'id');
    }

    public function device()
    {
        return $this->belongsTo(TrackerDevice::class, 'device_id', 'id');
    }

    public function agent()
    {
        return $this->belongsTo(TrackerAgent::class, 'agent_id', 'id');
    }

}
