<?php

namespace Quantumh\Visits\Models;

use Illuminate\Database\Eloquent\Model;

class TrackerLanguage extends Model
{
    protected $table = 'tracker_languages';

    protected $fillable = ['preference', 'language_range'];
}
