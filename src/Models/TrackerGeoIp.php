<?php

namespace Quantumh\Visits\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class TrackerGeoIp extends Model
{
    protected $table = 'tracker_geoip';

    protected $fillable = [
        'country_code',
        'country_name',
        'region_code',
        'region_name',
        'city_name',
        'zip_code',
        'iso_code',
        'postal_code',
        'latitude',
        'longitude',
        'area_code',
        'metro_code',
        'timezone'
    ];

}
