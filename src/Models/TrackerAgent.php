<?php

namespace Quantumh\Visits\Models;

use Illuminate\Database\Eloquent\Model;

class TrackerAgent extends Model
{
    protected $table = 'tracker_agents';

    protected $fillable = [
        'name',
        'browser',
        'browser_version',
        'device',
        'device_brand',
        'device_model',
        'name_hash',
    ];
}
