<?php

namespace Quantumh\Visits\Models;

use Illuminate\Database\Eloquent\Model;

class TrackerDevice extends Model
{
    protected $table = 'tracker_devices';

    protected $fillable = [
        'kind',
        'model',
        'brand',
        'platform',
        'platform_version',
        'is_mobile',
    ];
}
