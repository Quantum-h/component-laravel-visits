<?php

namespace Quantumh\Visits\Models;

use Illuminate\Database\Eloquent\Model;

class TrackerReferer extends Model
{
    protected $table = 'tracker_referers';

    protected $fillable = [
        'url',
        'host',
        'domain',
        'medium',
        'source',
        'search_terms_hash',
    ];
}
