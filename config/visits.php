<?php

return [
    'enabled' => env('TRACKER_VISITS_ENABLED',true),
    'tracker_cookie_name' => 'tracker_visits',
    'auth_table' => 'users',
    'auth_table_primary_key' => 'id',
    'auth_table_regionable_column' => 'region_id',
    'middleware' => [
        'set_visitor_uuid_cookie' => Quantumh\Visits\Http\Middleware\SetVisitorUuidCookie::class,
        'visit' => Quantumh\Visits\Http\Middleware\TrackVisit::class,
    ],

];
