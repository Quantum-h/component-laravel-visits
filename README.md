Register all visits on your Laravel app

## Installation

You can install the package via composer:

```bash
composer require quantumh/laravel-visits
```
The package will automatically register itself.

You can publish and run the migrations with:

```bash
php artisan vendor:publish --tag="quantum-visits-migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --tag="quantum-visits-config"
```

Add this key on your phpunit.xml to avoid make the test failing due to the visits.

```xml
 <env name="TRACKER_VISITS_ENABLED" value="false"/>
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Credits

- [Carlos Rodriguez]
- [David Vazquez]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
